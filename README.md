ansible-gitlab-ce
=========

Install and setup GitLab CE for RHEL and friends.

**_Visit the HTTP port of the server when done._**

##### Notes:

-   NOT INTENDED FOR:
    -   HA configurations or setting those up
    -   Backups or restoring
-   Uses official GitLab packages built from the [omnibus-gitlab](https://github.com/gitlabhq/omnibus-gitlab) project hosted on [PackageCloud](https://packages.gitlab.com/gitlab/gitlab-ce/install#manual)
-   Sets the system up to the point of having to visit the HTTP port of the server and set the gitlab root (administrator) password

Requirements
------------

-   A pristine and updated RHEL or CentOS 7 system

Role Variables
--------------
| Variable                              | Default |
|---------------------------------------|---------|
| `gitlab_reconfigure`                  | `yes`   |
| `firewalld_remove_unmanaged_services` | `yes`   |
| `firewalld_zone`                      | `Null`  |

Dependencies
------------

None.

Example Playbook
----------------

```yaml
---
- hosts: all
  become: yes

  roles:
    - ansible-role-gitlab-ce
```

License
-------

Apache 2.0

Author Information
------------------

Michael Goodwin
